# Montage

![montage_sankey](montage_sankey_v0.png)

This repo contains development code for Montage astronomical image processing workflows which include basic examples forked from:

- example 1: https://github.com/hariharan-devarajan/iopp/blob/dev/apps/montage/montage_2mass_ngc_3372_mosaic_lassen_iopp.sh, 
  - this is based on IBM LSF batch script without pegasus/HTCondor
  - and the paper: Extracting and characterizing I/O behavior of HPC workloads, https://www.osti.gov/servlets/purl/1878443
- example 2: https://github.com/pegasus-isi/montage-workflow-v3
  - running with Pegasus WMS
 
## Getting started on BlueSky

Path to software:
- Pegasus 5.0: `/files0/oddite/leeh736/pegasus-5.0.2`
- Montage 6.0: `/files0/oddite/leeh736/Montage`    
- Workflow Example (code 2): `/files0/oddite/leeh736/montage-workflow-v3`

Envs to activate:
- Module required to load: `module load java/11.0.14.1_1`
- Path to enable: `export PATH=$PATH:/files0/oddite/leeh736/Montage/bin/:/files0/oddite/leeh736/pegasus-5.0.2/bin/`

### Package Requirements

Python packages work with compiled binary installations and these are necessage to have in your python environment:

- MontagePy
- astropy
- graphviz


## QuickStart

- `example-dss.sh` prepared input files to run a montage workflow with basic
  settings that create a 2x2 degree mosaic centered on 56.7 24.0, with single
band for the final JPEG output. A 1 degree is applied which will reduce a
number of tasks to process relatively small number of input images and thus the
workflow finishes quickly with a small number of tasks.
- `data` sub-directory will be created which contains Pegasus workflow DAX in a
  yaml format.
- `work` sub-directory will be created which contains DAG for actual execution
  plan e.g., including data staging. 

## Sample DAG for Montage Task (job) Dependency

![workflow job dependency](montage-job-dep.png)

